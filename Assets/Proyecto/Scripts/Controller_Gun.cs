using System.Collections;
using System.Collections.Generic;
using UnityEngine.VFX;
using UnityEngine;

public class Controller_Gun : MonoBehaviour
{
    #region Variables
    public Player_Controller controller;
    public Gun[] gunis;
    public Gun actualGun;
    public int indexGun = 0;
    public int maxGuns = 3;

    public VisualEffect balas;
    public AudioClip audioClip;
    public AudioClip clip;
    float lastShootTime = 0;
    Vector3 currentRotation;
    Vector3 targetRotation;
    public float retornSpeed;
    public float snppines;
    public GameObject prefBulltHole;
    float lastTime;
    float lastChangeTime;
    public float changeTime;
    float recoger;
    bool reload;
    bool isChange;
    public float distancia;

    #endregion

    #region Funciones
    private void Update()
    {
        if(actualGun != null)
        {
            if(lastShootTime <= 0)
            {
                if (!actualGun.data.automatic)
                {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
                else
                {
                    if (Input.GetButton("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
            }
            if (Input.GetButtonDown("Reload") && !reload)
            {
                if (actualGun.data.actualAmmo < actualGun.data.maxAmmoCount)
                {
                    lastTime = 0;
                    reload = true;
                }
                if(actualGun.data.actualAmmo >= 0)
                {
                    Reload();
                }
            }
            if(Input.GetButtonDown("F")&& !reload && isChange)
            {
                Drop();
            }
        }
        else
        {

        }
        if(lastShootTime  >= 0)
        {
            lastShootTime -= Time.deltaTime;
        }
        if (reload)
        {
            lastTime += Time.deltaTime;
            if (lastTime >= actualGun.data.reloadTime)
            {
                reload = false;
                Reload();
            }
        }
        targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, retornSpeed * Time.deltaTime);
        currentRotation = Vector3.Slerp(currentRotation, targetRotation, snppines * Time.deltaTime);
        controller.recoil.localRotation = Quaternion.Euler(currentRotation);
        if (Input.GetButtonDown("Gun1") && !reload)
        {
            if (indexGun != 0)
            {
                indexGun = 0;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChange = true;
            }
        }
        if (Input.GetButtonDown("Gun2") && !reload)
        {
            if (indexGun != 1)
            {
                indexGun = 1;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChange = true;
            }
        }
        if (Input.GetButtonDown("Gun3") && !reload)
        {
            if (indexGun != 2)
            {
                indexGun = 2;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChange = true;
            }
        }
        if (isChange)
        {
            lastChangeTime += Time.deltaTime;
            if (lastChangeTime >= changeTime)
            {
                isChange = false;
                ChangeGun(indexGun);
            }
        }
        if(Physics.Raycast(controller.cam.transform.position, controller.cam.transform.forward, out RaycastHit hit, distancia))
        {
            if(hit.transform.tag == ("Gun"))
            {
                if(Input.GetButtonDown("F") && !reload && !isChange)
                {
                    GetGun(hit.transform.GetComponent<Gun>());

                }
            }
        }
    }
    #endregion

    #region Custom Functions
    private void Shoot()
    {
        if(reload == false)
        {
            if (Physics.Raycast(controller.cam.transform.position, controller.cam.transform.forward, out RaycastHit hit, actualGun.data.range))
            {
                if (hit.transform != null)
                {
                    Debug.Log($"te di : {hit.transform.name}");
                    GameObject preb = Instantiate(prefBulltHole, hit.point + hit.normal * 0.001f, Quaternion.LookRotation(hit.normal, Vector3.up));
                    Destroy(preb, 2f);
                }
            }
            actualGun.data.actualAmmo--;
            lastShootTime = actualGun.data.fireRate;
            Addrecoil();
            GameObject gp = VisualEffect.Instantiate(balas, actualGun.almo.position,actualGun.almo.rotation ).gameObject;
            gp.transform.parent = actualGun.almo;
            actualGun.audioSource.PlayOneShot(actualGun.audioClip);
            Destroy(gp, 2f);
        }
    }
    void Addrecoil()
    {
        targetRotation -= new Vector3(actualGun.data.recoil.x, Random.Range(-actualGun.data.recoil.y, actualGun.data.recoil.y),0f);
    }
    void Reload()
    {
        actualGun.data.actualAmmo = actualGun.data.maxAmmoCount;
        actualGun.audioSource.PlayOneShot(clip);
    }
    void ChangeGun( int index)
    {
        if (gunis[index] != null)
        {
            actualGun = gunis[index];
            actualGun.gameObject.SetActive(true);
            actualGun.audioSource.PlayOneShot(audioClip);
        }
    }
    void Drop()
    {
        actualGun.GetComponent<Rigidbody>().useGravity = false;
        actualGun.GetComponent<Rigidbody>().isKinematic = true;
        actualGun.gameObject.transform.SetParent(null);
        actualGun = null;
        gunis[indexGun] = null;
    }
    void GetGun(Gun gun)
    {
        if(actualGun != null)
        {
            Drop();
        }
        actualGun = gun;
        actualGun.GetComponent<Rigidbody>().useGravity = false;
        actualGun.GetComponent<Rigidbody>().isKinematic = true;
        actualGun.transform.parent = controller.gunPoint;
        actualGun.transform.localPosition = actualGun.data.offset;
        actualGun.transform.localRotation = Quaternion.identity;
        gunis[indexGun] = actualGun;
    }
    #endregion
}
