using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{
    #region Variables
    internal Camera cam;
    public Transform recoil;
    public Transform gunPoint;
    [SerializeField] private CharacterController controller;
    [SerializeField] private Transform pointOfView;
    [SerializeField] private float walkSpeed = 5;
    [SerializeField] private float runSpeed = 8;
    [SerializeField] private float jumpForze = 12;
    [SerializeField] private float gravityMod = 2.5f;

    private float actualSpeed;
    private float horizontalRotationStore;
    private float verticalRotationStore;
    private Vector2 mouseInput;
    private Vector3 direccion;
    private Vector3 movement;

    [Header("Ground Deteccion")]
    [SerializeField] private bool isGround;
    [SerializeField] private float radio;
    [SerializeField] private float distance;
    [SerializeField] private Vector3 offSet;
    [SerializeField] private LayerMask lm;
    #endregion

    #region Unity Functions
    public void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        controller.GetComponent<CharacterController>();
        cam = Camera.main;
    }
    public void Update()
    {
        Rotation();
        Movement();
        IsGrounded();
    }
    private void LateUpdate()
    {
        cam.transform.position = recoil.position;
        cam.transform.rotation = recoil.rotation;
        gunPoint.transform.position = recoil.position;
        gunPoint.transform.rotation = recoil.rotation;
    }
    #endregion

    #region Custom Functions
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + offSet, radio);
        if (Physics.SphereCast(transform.position + offSet, radio, Vector3.down, out RaycastHit hit, distance, lm))
        {
            Gizmos.color = Color.green;
            Vector3 onPoint = (transform.position + offSet) + (Vector3.down * distance);
            Gizmos.DrawWireSphere(onPoint, radio);
            Gizmos.DrawLine(transform.position + offSet, onPoint);
            Gizmos.DrawSphere(hit.point, radio);
        }
        else
        {
            Gizmos.color = Color.red;
            Vector3 onPoint = (transform.position + offSet) + (Vector3.down * distance);
            Gizmos.DrawWireSphere(onPoint, radio);
            Gizmos.DrawLine(transform.position + offSet, onPoint);
        }
    }
    private void Rotation()
    {
        mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"),Input.GetAxisRaw("Mouse Y"));
        horizontalRotationStore += mouseInput.x;
        verticalRotationStore -= mouseInput.y;
        verticalRotationStore = Mathf.Clamp(verticalRotationStore, -60f, 60f);
        transform.rotation = Quaternion.Euler(0f,horizontalRotationStore,0f);
        pointOfView.transform.localRotation = Quaternion.Euler(verticalRotationStore, 0f, 0f);
    }
    private void Movement()
    {
        direccion = new Vector3(Input.GetAxis("Horizontal"),0, Input.GetAxis("Vertical"));
        float volY = movement.y;
        movement = ((transform.forward * direccion.z) + (transform.right * direccion.x)).normalized;
        movement.y = volY;
        if(Input.GetButton("Fire3"))
        {
            actualSpeed = runSpeed;
        }
        else
        {
            actualSpeed = walkSpeed;
        }
        if(IsGrounded())
        {
            movement.y = 0;
        }
        if(Input.GetButtonDown("Jump") && controller.isGrounded)
        {
            movement.y = jumpForze * Time.deltaTime;
        }
        movement.y += Physics.gravity.y * Time.deltaTime * gravityMod;
        controller.Move(movement * (actualSpeed * Time.deltaTime));
    }
    private bool IsGrounded()
    {
        isGround = false;
        if(Physics.SphereCast(transform.position + offSet, radio, Vector3.down, out RaycastHit hit,distance, lm))
        {
            isGround = true;
        }
        return isGround;
    }
    #endregion
}
