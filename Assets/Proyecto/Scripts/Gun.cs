using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    #region Varibles
    public GunData data;
    public Transform almo;
    public AudioClip audioClip;
    public AudioSource audioSource;
    #endregion
    #region Custom Functions
    private void Awake()
    {
        data.actualAmmo = data.maxAmmoCount;
    }
    #endregion
}
