using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;

public class Launcher : MonoBehaviourPunCallbacks
{
    #region Variables
    public static Launcher Instance;
    [SerializeField] private GameObject[] screenObjets;
    [SerializeField] TMP_Text infoText;
    [SerializeField] TMP_Text room;
    [SerializeField] TMP_Text error_Text;
    [SerializeField] TMP_InputField field;
    [SerializeField] GameObject gameSala;
    [SerializeField] Transform roomButtonParent;
    [SerializeField] List<RoomButton> rooms = new List<RoomButton>();
    #endregion

    #region 
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

    }
    private void Start()
    {
        infoText.text = "Connecting to Network...";
        SetScreenObjets(0);
        PhotonNetwork.ConnectUsingSettings();

    }
    #endregion
    #region Cunstom fanctions
    public void SetScreenObjets(int index)
    {
        for (int i = 0; i < screenObjets.Length; i++)
        {
            screenObjets[i].SetActive(i == index);
        }
    }
    public void CreateRoom()
    {
        if(!string.IsNullOrEmpty(field.text))
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = 10;
            PhotonNetwork.CreateRoom(field.text);
            infoText.text = "Creating Room...";
            SetScreenObjets(0);
        }
    }
    public void Back()
    {
        PhotonNetwork.LeaveRoom();
        infoText.text = "leave Room";
        SetScreenObjets(1);
    }
    public void JoinRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
        infoText.text = "Joining Room...";
        SetScreenObjets(0);
    }
    #endregion
    #region Photon
    public override void OnLeftRoom()
    {
        SetScreenObjets(1);
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            if(roomList[i].RemovedFromList)
            {
                for (int j = 0; j < rooms.Count; j++)
                {
                    if(roomList[i].Name == rooms[j].info.Name)
                    {
                        GameObject go = rooms[j].gameObject;
                        rooms.Remove(rooms[j]);
                        Destroy(go);
                    }
                }
            }
        }
        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].PlayerCount != roomList[i].MaxPlayers && !roomList[i].RemovedFromList)
            {
                RoomButton newroomButton = Instantiate(gameSala, roomButtonParent).GetComponent<RoomButton>();
                newroomButton.SetButtonDetalls(roomList[i]);
                rooms.Add(newroomButton);
                Debug.Log(roomList);
            }
        }
    }
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(); 
        SetScreenObjets(1);
    }
    public override void OnJoinedRoom()
    {
       room.text =  PhotonNetwork.CurrentRoom.Name;
        SetScreenObjets(3);
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        error_Text.text = "Error al crear la sala, ya existe" + message;
        SetScreenObjets(4);
    }
    #endregion
}
