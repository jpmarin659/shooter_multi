using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;

public class RoomButton : MonoBehaviour
{
    #region Variables
    [SerializeField] TMP_Text textOne;
    public RoomInfo info;

    #endregion

    #region Cunstom Factions
    public void SetButtonDetalls(RoomInfo inputInfo )
    {
        info = inputInfo;
        textOne.text = info.Name;
    }
    public void JoinRoom()
    {
        Launcher.Instance.JoinRoom(info);
    }
    #endregion

    #region Photon

    #endregion
}
